//buildTree(nodes);

var buildTree = function (nodes) {
	var treeNodes = convertToHierarchy(nodes, { ParentId: null });
	populateTreeNode(document.getElementById("treecontent"), treeNodes, treeNodes.ParentId);
};

var convertToHierarchy = function(nodes, condition) {
	var roots = [];

	_.each(_.where(nodes, condition), function(node) {
	    node.children = convertToHierarchy(nodes, { ParentId: node.Id });
		roots.push(node);
		delete node;
	});

	return roots;
};

var populateTreeNode = function(parentNode, treeNodes, level) {
	_.each(treeNodes, function(node) {
		var container = document.createElement('li');
		container.setAttribute('data-id', node.Id);
		if (level == undefined)
		    container.classList.add("listTreeDau");

		var nodeLink = document.createElement('a');
		nodeLink.setAttribute('href', "#");
		nodeLink.innerHTML = node.Name;

		container.appendChild(nodeLink);
		parentNode.appendChild(container);

		var childContainer = document.createElement('ul');
		container.appendChild(childContainer);

		if (node.children != null && node.children.length > 0) {
		    container.classList.add("listTree");
			populateTreeNode(childContainer, node.children, level++);
		}
	});
};
