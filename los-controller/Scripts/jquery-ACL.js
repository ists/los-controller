﻿

function SetAcl(Siddata, ACLdata) {
    var sysright = {
        Sid: Siddata,
        Acl: ACLdata
    };
    //Check existent Sid
    var Sidarr = $.parseJSON($('#SysrightHidden').val());
    console.log(Sidarr);
    console.log(Siddata);
    var existent = jLinq.from(Sidarr).equals('Sid', Siddata).count();
    console.log(existent);
    if (existent !== 0) {
        alert("Sid existent !");
        return true;
    }
    else {

        Sidarr.push(sysright);
        $('#SysrightHidden').val(JSON.stringify(Sidarr));
        console.log($('#SysrightHidden').val());
        return false;
    }
}


function DeleteACl(Siddata) {
    var data = $.parseJSON($('#SysrightHidden').val());
    var deleteSid = jLinq.from(data).notEquals("Sid", Siddata).select();
    console.log(deleteSid);
    $('#SysrightHidden').val(JSON.stringify(deleteSid));
    console.log($('#SysrightHidden').val());
}


function RemoveRight(inputSid, right) {
    var data = $.parseJSON($('#SysrightHidden').val());
    var model = jLinq.from(data).equals("Sid", inputSid).select();
    model[0].Acl = model[0].Acl.replace(right, '');
    var dataupdate = jLinq.from(data).notEquals("Sid", inputSid).select();
    dataupdate.push(model[0]);
    console.log(dataupdate);
    $('#SysrightHidden').val(JSON.stringify(dataupdate));
}



function AddRight(inputSid, right) {
    var data = $.parseJSON($('#SysrightHidden').val());
    var model = jLinq.from(data).equals("Sid", inputSid).select();
    model[0].Acl = model[0].Acl + right;
    var dataupdate = jLinq.from(data).notEquals("Sid", inputSid).select();
    dataupdate.push(model[0]);
    console.log(dataupdate);
    $('#SysrightHidden').val(JSON.stringify(dataupdate));
}

function getSelectValues(select) {
    var result = "";
    var options = select && select.options;
    var opt;

    for (var i = 0, iLen = options.length; i < iLen; i++) {
        opt = options[i];
        if (opt.selected) {
            result = result + (opt.value);
        }
    }
    return result;
}




function DisplayspanUser(Sid, rights) {
    var displayspan = '<li data-id="' + Sid + '" class="border-bottom-1-2"><div class="nameUser"><a href="#" class="icon-user">' + Sid + '</a></div><div class="quyenUser">';
    if (rights.indexOf('V') > -1) {

        displayspan = displayspan + '<span data-quyen="V" class="border-1 applyQuyen">View</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="V" class="border-1">View</span>';
    }
    if (rights.indexOf('I') > -1) {
        displayspan = displayspan + '<span data-quyen="I" class="border-1 applyQuyen">Input</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="I" class="border-1">Input</span>';
    }
    if (rights.indexOf('U') > -1) {
        displayspan = displayspan + '<span data-quyen="U" class="border-1 applyQuyen">Update</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="U" class="border-1">Update</span>';
    }
    if (rights.indexOf('X') > -1) {
        displayspan = displayspan + '<span data-quyen="X" class="border-1 applyQuyen">Execute</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="X" class="border-1">Execute</span>';
    }
    if (rights.indexOf('D') > -1) {
        displayspan = displayspan + '<span data-quyen="D" class="border-1 applyQuyen">Delete</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="D" class="border-1">Delete</span>';
    }
    if (rights.indexOf('A') > -1) {
        displayspan = displayspan + '<span data-quyen="A" class="border-1 applyQuyen">Approve</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="A" class="border-1">Approve</span>';
    }
    displayspan = displayspan + '</div><div class="deleteUser"><span class="removeUser icon-close"></span></div></li>';
    return displayspan;
}

function DisplayspanGroup(Sid, rights) {
    var displayspan = '<li data-id="' + Sid + '" class="border-bottom-1-2"><div class="nameGroup"><a href="#" class="icon-users">' + Sid + '</a></div><div class="quyenGroup">';
    if (rights.indexOf('V') > -1) {

        displayspan = displayspan + '<span data-quyen="V" class="border-1 applyQuyen">View</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="V" class="border-1">View</span>';
    }
    if (rights.indexOf('I') > -1) {
        displayspan = displayspan + '<span data-quyen="I" class="border-1 applyQuyen">Input</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="I" class="border-1">Input</span>';
    }
    if (rights.indexOf('U') > -1) {
        displayspan = displayspan + '<span data-quyen="U" class="border-1 applyQuyen">Update</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="U" class="border-1">Update</span>';
    }
    if (rights.indexOf('X') > -1) {
        displayspan = displayspan + '<span data-quyen="X" class="border-1 applyQuyen">Execute</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="X" class="border-1">Execute</span>';
    }
    if (rights.indexOf('D') > -1) {
        displayspan = displayspan + '<span data-quyen="D" class="border-1 applyQuyen">Delete</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="D" class="border-1">Delete</span>';
    }
    if (rights.indexOf('A') > -1) {
        displayspan = displayspan + '<span data-quyen="A" class="border-1 applyQuyen">Approve</span>';
    }
    else {
        displayspan = displayspan + '<span data-quyen="A" class="border-1">Approve</span>';
    }
    displayspan = displayspan + '</div><div class="deleteGroup"><span class="removeGroup icon-close"></span></div></li>';
    return displayspan;
}