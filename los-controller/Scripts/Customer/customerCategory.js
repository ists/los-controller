$(document).ready(function () {
    $(this).addEventHeader();
    $('#input-seach-product').keyup(function (e) {
        var textSeach = this.value;
        if (this.value == ' ') {
            this.value = '';
        } else if (this.value !== '') {
            $('.listSeachTree').remove();
            var ul = $('<ul class="listSeachTree"></ul>');
            $('#treeBody ul').first().find('a').each(function (index, value) {
                if (value.text.indexOf(textSeach) !== -1) {
                    ul.append($(value).parent().clone().removeClass('listTree listTreeDau').click(function () {
                        $('.active-tree').removeClass('active-tree');
                        $(this).children('a').addClass('active-tree');
                        var loading = $().loading();
                        $('#content-product').append(loading);
                        setTimeout(function () {
                            $.get('main-product.html', function (data) {
                                loading.remove();
                                $('#body-product').html(data).addEventProduct();
                                svgcheck();
                            }, 'html');
                        }, 800);
                        e.stopPropagation();
                    }).each(function (index, item) {
                        return $(item).children('ul').remove();
                    }));
                }
            });
            $('#treeBody').find('ul').hide().end().append(ul);
        } else {
            $('#treeBody ul').first().find('a').each(function (index, value) {
                if ($(value).text() === $('.active-tree').text()) {
                    $(value).addClass('active-tree');
                }
            });
            $('.active-tree').parents('#treeBody ul').show();
            $('.listSeachTree').remove();
            $('#treeBody > ul').show();
        }

    });




    /* event Product
	---------------------------------------------------------------------------------------*/
    $.get("/CustomerCategory/GetCustCatTree", function (output) {
        console.log(output);
        console.log(JSON.stringify(output));
        buildTree(output);

        $('#treeBody li').click(function (e) {
            console.log(this);
            if ($(this).hasClass('listTree') && $(this).hasClass('selectTree')) {
                $(this).removeClass('selectTree').children('ul').hide();
            } else if ($(this).hasClass('listTree')) {
                $(this).addClass('selectTree').children('ul').show();
            }
            console.log('li');
            return false;
        });

        $('#treeBody a').click(function (e) {
            $('.active-tree').removeClass('active-tree');
            $(this).addClass('active-tree');
            $('#body-product').remove();
            var loading = $().loading();
            $('#content-product').append(loading);
            console.log(e);
            console.log('ham Ajax View product');
            setTimeout(function () {
                $.get('CustomerCategory/GetCustCatDetail/?CustCatID=' + $(this).treeActive('.active-tree').data('id'), function (data) {
                    loading.remove();
                    $('#content-product').html(data).addEventProduct();
                    svgcheck();
                }, 'html');
            }, 800);
            return false;
        });


        $('#new-product').click(function (e) {
            $('.titleTab').remove();
            $('#body-product > div').fadeOut(200);
            $('#combobox').remove();
            var loading = $().loading();
            $('#content-product').append(loading);

            setTimeout(function () {
                $.get('CustomerCategory/NewCustomerCatgory', function (data) {
                    loading.remove();
                    $('#body-product').append(data).addEventCombobox({
                        getUrl: 'link.get/post (newProduct)',
                        addUserUrl: 'links Add User (newProduct)',
                        addGroupUrl: 'links add Group (newProduct)'
                    });
                    svgcheck();
                }, 'html');
            }, 800);

            $('#box-header-product').append($('<div class="titleTab activeMenu">New Customer Category</div>'));

            console.log('new product');
            e.stopPropagation();
        });

        /* event new/edit/delete product */
       

        //$('#edit-product').click(function (e) {
        //    var tree = document.querySelector('.active-tree');
        //    if (tree !== null) {
        //        $('.titleTab').remove();
        //        $('#body-product > div').fadeOut(200);
        //        $('#combobox').remove();
        //        var loading = $().loading();
        //        $('#content-product').append(loading);
        //        setTimeout(function () {
        //            $.get('Product/EditProduct/?ProductId=' + $(this).treeActive('.active-tree').data('id'), function (data) {
        //                loading.remove();
        //                $('#body-product').append(data).addEventCombobox({
        //                    getUrl: 'link.get/post (editProduct)',
        //                    addUserUrl: 'links Add User (editProduct)',
        //                    addGroupUrl: 'links add Group (editProduct)'
        //                });
        //                svgcheck();
        //            }, 'html');
        //        }, 800);

        //        $('#box-header-product').append($('<div class="titleTab activeMenu">Edit Product</div>'));
        //    }

        //    e.stopPropagation();
        //});

        //$('#delete-product').click(function (e) {
        //    var tree = document.querySelector('.active-tree');
        //    if (tree !== null) {
        //        var $this = $(this);
        //        $('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Tree này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');

        //        $('#DeleteTree span').click(function (e) {
        //            if ($(this).hasClass('yesDelete')) {
        //                $.ajax({
        //                    url: 'Product/deleteProduct',
        //                    data: { productId: $(this).treeActive('.active-tree').data('id') },
        //                    type: 'POST',
        //                    success: function (result) {
        //                        $('.titleTab').remove();
        //                        $this.treeActive('.active-tree').remove();
        //                        $('#body-product').html('');
        //                        console.log('viet Layout delete product');
        //                    },
        //                    error: function () {
        //                    }
        //                });
        //            }
        //            $('#layoutDeleteTree').remove();
        //            e.stopPropagation();
        //        });
        //    }
        //    e.stopPropagation();
        //});
    });
});