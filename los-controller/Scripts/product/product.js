﻿$(document).ready(function () {
    $(this).addEventHeader();
    $('#input-seach-product').keyup(function (e) {
        var textSeach = this.value;
        if (this.value == ' ') {
            this.value = '';
        } else if (this.value !== '') {
            $('.listSeachTree').remove();
            var ul = $('<ul class="listSeachTree"></ul>');
            $('#treeBody ul').first().find('a').each(function (index, value) {
                if (value.text.indexOf(textSeach) !== -1) {
                    ul.append($(value).parent().clone().removeClass('listTree listTreeDau').click(function () {
                        $('.active-tree').removeClass('active-tree');
                        $(this).children('a').addClass('active-tree');
                        var loading = $().loading();
                        $('#content-product').append(loading);
                        setTimeout(function () {
                            $.get('main-product.html', function (data) {
                                loading.remove();
                                $('#body-product').html(data).addEventProduct();
                                svgcheck();
                            }, 'html');
                        }, 800);
                        e.stopPropagation();
                    }).each(function (index, item) {
                        return $(item).children('ul').remove();
                    }));
                }
            });
            $('#treeBody').find('ul').hide().end().append(ul);
        } else {
            $('#treeBody ul').first().find('a').each(function (index, value) {
                if ($(value).text() === $('.active-tree').text()) {
                    $(value).addClass('active-tree');
                }
            });
            $('.active-tree').parents('#treeBody ul').show();
            $('.listSeachTree').remove();
            $('#treeBody > ul').show();
        }

    });




    /* event Product
	---------------------------------------------------------------------------------------*/
    $.get("/Product/getProductTree", function (output) {
        console.log(output);
        console.log(JSON.stringify(output));
        buildTree(output);
        var ajax = null;
        $.ajax({
            type: 'POST',
            url: 'Product/getProductDetail',
            ifModified: true,
            cache: false,
            data: { productId: $('#treeBody li:first').data('id') },
            success: function (response) {
                $('#treeBody li:first').addClass('selectTree').children('ul').show();
                $('#treeBody li:first a:first').addClass('active-tree');
                $('#body-product').remove();
                $('#content-product').html(response).addEventProduct();
                svgcheck();
            }
        });


        $('#treeBody li').click(function (e) {
            console.log(this);
            if ($(this).hasClass('listTree') && $(this).hasClass('selectTree')) {
                $(this).removeClass('selectTree').children('ul').hide();
            } else if ($(this).hasClass('listTree')) {
                $(this).addClass('selectTree').children('ul').show();
            }
            console.log('li');
            return false;
        });

        $('#treeBody a').click(function (e) {
            $('.active-tree').removeClass('active-tree');
            $(this).addClass('active-tree');
            $('#body-product').remove();
            var loading = $().loading();
            $('#content-product').append(loading);
            console.log(e);
            if (ajax)
                ajax.abort();
             ajax= $.ajax({
                    type: 'POST',
                    url: 'Product/getProductDetail',
                    ifModified: true,
                    cache : false,
                    data: { productId: $(this).treeActive('.active-tree').data('id') },
                    success: function(response){
                        loading.remove();
                        $('#content-product').html(response).addEventProduct();
                        svgcheck();
                    }
                });

            return false;
        });

        $('#new-product').click(function (e) {
            $('.titleTab').remove();
            $('#body-product > div').fadeOut(200);
            $('#combobox').remove();
            var loading = $().loading();
            $('#content-product').append(loading);
            localStorage.clear();
            setTimeout(function () {
                $.get('Product/NewProduct', function (data) {
                    loading.remove();
                    $('#body-product').append(data).addEventCombobox({
                        getUrl: 'link.get/post (newProduct)',
                        addUserUrl: 'links Add User (newProduct)',
                        addGroupUrl: 'links add Group (newProduct)'
                    });
                    svgcheck();
                }, 'html');
            }, 800);

            $('#box-header-product').append($('<div class="titleTab activeMenu">New Product</div>'));

            console.log('new product');
            e.stopPropagation();
        });
    });



});