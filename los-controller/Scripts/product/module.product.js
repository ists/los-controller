(function ($) {

    $.fn.extend({
        treeActive: function () {
            return $('.active-tree').parent();
        },
        addEventProduct: function () {

            /* event tab-product */
            $('#tab-view span').click(function (e) {
                $('.active').removeClass('active');
                $(this).addClass('active');
                if (this.id === 'tab-ACL') {
                    $('#body-main-product').hide();
                    $('#body-ACL-product').show();
                } else if (this.id === 'tab-main-UDF') {
                    $('#body-ACL-product').hide();
                    $('#body-main-product').show();
                }
                e.stopPropagation();
            });

            /* event new/edit/delete product */
            $('#new-product').click(function (e) {
                $('.titleTab').remove();
                $('#body-product > div').fadeOut(200);
                $('#combobox').remove();
                var loading = $().loading();
                $('#content-product').append(loading);
                localStorage.clear();
                setTimeout(function () {
                    $.get('Product/NewProduct/ProductId=' + $(this).treeActive('.active-tree').data('id'), function (data) {
                        loading.remove();
                        $('#body-product').append(data).addEventCombobox({
                            getUrl: 'link.get/post (newProduct)',
                            addUserUrl: 'links Add User (newProduct)',
                            addGroupUrl: 'links add Group (newProduct)'
                        });
                        svgcheck();
                    }, 'html');
                }, 800);

                $('#box-header-product').append($('<div class="titleTab activeMenu">New Product</div>'));

                console.log('new product');
                e.stopPropagation();
            });

            $('#edit-product').click(function (e) {
                var tree = document.querySelector('.active-tree');
                if (tree !== null) {
                    $('.titleTab').remove();
                    $('#body-product > div').fadeOut(200);
                    $('#combobox').remove();
                    var loading = $().loading();
                    $('#content-product').append(loading);
                    setTimeout(function () {
                        $.get('Product/EditProduct/?ProductId=' + $(this).treeActive('.active-tree').data('id'), function (data) {
                            loading.remove();
                            $('#body-product').append(data).addEventCombobox({
                                getUrl: 'link.get/post (editProduct)',
                                addUserUrl: 'links Add User (editProduct)',
                                addGroupUrl: 'links add Group (editProduct)'
                            });
                            svgcheck();
                        }, 'html');
                    }, 800);

                    $('#box-header-product').append($('<div class="titleTab activeMenu">Edit Product</div>'));
                }

                e.stopPropagation();
            });

            $('#delete-product').click(function (e) {
                var tree = document.querySelector('.active-tree');
                if (tree !== null) {
                    var $this = $(this);
                    $('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Tree này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');

                    $('#DeleteTree span').click(function (e) {
                        if ($(this).hasClass('yesDelete')) {
                            $.ajax({
                                url: 'Product/DeleteProduct',
                                data: { productId: $(this).treeActive('.active-tree').data('id') },
                                type: 'POST',
                                success: function (result) {
                                    alert("Successful");
                                    $('.titleTab').remove();
                                    $('#body-product').html('');
                                    console.log('viet Layout delete product');
                                },
                                error: function () {
                                }
                            });
                        }
                        $('#layoutDeleteTree').remove();
                        e.stopPropagation();
                    });
                }
                e.stopPropagation();
            });
        },
        addEventCombobox: function () {
            var setting = arguments[0];
            var browser = $().browserName();
            console.log(setting);

            if (!(browser === 'Chrome' || browser === 'Safari' || browser === 'Opera')) {
                $(function () {
                    $("input[type='date']").datepicker({
                        dateFormat: 'yy-mm-dd',
                        onClose: function (dateText, inst) {
                            $(this).valid()
                        }
                    });
                });
            };

            $('select:not([name="quyenUser"],[name="quyenGroup"])').msDropdown({ roundedBorder: false });
            $('.changeACL').hide();

            $('#tab-view-new-edit span').click(function () {
                $('.actived').removeClass('actived');
                $(this).addClass('actived');
                if (this.id === 'tab-ACL') {
                    $('.change-Main-Udf').hide();
                    $('.changeACL').show();
                } else if (this.id === 'tab-main-UDF') {
                    $('.changeACL').hide();
                    $('.change-Main-Udf').show();
                }
            });

            $('#exit-combobox').click(function (e) {
                $('.titleTab').remove();
                $('#combobox').fadeOut(300).remove();
                $('#body-product > div').show();
                $('.quyenUser span').unbind('click');
                $('.quyenGroup span').unbind('click');
                e.stopPropagation();
            });

            //$('#save-product').click( function( e ) {
            //	$('#exit-combobox').click();
            //	var idproduct = $("#treeBody").getActiveNode().parent().data('id');

            //	console.log(idproduct);

            //	console.log('viet ham Ajax $.get(', setting.getUrl, ')');
            //	$.post(setting.getUrl, function( data ) {
            //		// viet ham post save product
            //	});

            //	e.stopPropagation();
            //});

            $('.addUser').click(function (e) {
                var form = document.formAddUser;
                var quyenaddUser = getSelectValues(document.formAddUser.quyenUser);
                if (form.userAdd.value != '' && form.quyenUser.value != '') {
                    var CheckAcl = SetAcl(form.userAdd.value, quyenaddUser);
                    if (!CheckAcl) {
                        var selectid = form.userAdd.id;
                        var spanright = DisplayspanUser(form.userAdd.value, quyenaddUser)
                        $('.listUser').prepend($(spanright).fadeIn(600));
                    }
                }

                $('.removeUser').unbind('click').bind('click', removeUser);
                $('.quyenUser span').unbind('click').bind('click', quyenUser);
                e.stopPropagation();
            });

            $('.addGroup').click(function (e) {
                var form = document.formAddGroup;
                var quyenaddGroupd = getSelectValues(document.formAddGroup.quyenGroup)
                if (form.selectGroup.value != '' && form.quyenGroup.value != '') {
                    var CheckAcl = SetAcl(form.selectGroup.value, quyenaddGroupd);
                    if (!CheckAcl) {
                        var spanright = DisplayspanGroup(form.selectGroup.value, quyenaddGroupd)
                        $('.listGroup').prepend($(spanright).fadeIn(600));
                    }
                }
                $('.removeGroup').unbind('click').bind('click', removeGroup);
                $('.quyenGroup span').unbind('click').bind('click', quyenGroup);
                e.stopPropagation();
            });

            $('.removeUser').click(removeUser);
            function removeUser(e) {
                var parent = $(this).parent().parent();
                $('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete User này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
                $('#DeleteTree span').click(function (e) {
                    $('#layoutDeleteTree').remove();
                    if ($(this).hasClass('yesDelete')) {
                        DeleteACl(parent.data('id'));
                        parent.fadeOut(800, function () {
                            $(this).remove();
                        });
                    }
                    e.stopPropagation();
                });

                e.stopPropagation();
            }

            $('.removeGroup').click(removeGroup);
            function removeGroup(e) {
                var parent = $(this).parent().parent();
                $('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Group này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
                $('#DeleteTree span').click(function (e) {
                    $('#layoutDeleteTree').remove();
                    if ($(this).hasClass('yesDelete')) {
                        console.log('viet ham Ajax remove Group co id =', parent.data('id'));
                        DeleteACl(parent.data('id'));
                        parent.fadeOut(600, function () {
                            $(this).remove();
                        });
                    }
                    e.stopPropagation();
                });

                e.stopPropagation();
            }

            $('.quyenUser span').bind('click', quyenUser);
            function quyenUser(e) {
                var idItems = $(this).parent().parent().data('id');
                var quyen = $(this).data('quyen');
                if ($(this).hasClass('applyQuyen')) {
                    RemoveRight(idItems, quyen);
                    $(this).removeClass('applyQuyen');
                } else {
                    AddRight(idItems, quyen);
                    $(this).addClass('applyQuyen');
                }
                e.stopPropagation();
            }

            $('.quyenGroup span').bind('click', quyenGroup);
            function quyenGroup(e) {
                var idItems = $(this).parent().parent().data('id');
                var quyen = $(this).data('quyen');
                if ($(this).hasClass('applyQuyen')) {
                    RemoveRight(idItems, quyen);
                    $(this).removeClass('applyQuyen');
                } else {
                    AddRight(idItems, quyen);
                    $(this).addClass('applyQuyen');
                }
                e.stopPropagation();
            }

            return false;
        }
    });

})(jQuery);