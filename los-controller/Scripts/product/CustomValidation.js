﻿$(function () {
    jQuery.validator.unobtrusive.adapters.add
        ("propertiesvalidation", ['other'], function (options) {
            //options.rules["propertiesvalidation"] = options.params.otherproperty;
            //options.messages["propertiesvalidation"] = options.message;
            var prefix = getModelPrefix(options.element.name);
            var other = options.params.other;
            var fullOtherName = appendModelPrefix(other, prefix);
            var element = $(options.form).find(":input").filter("[name='" + escapeAttributeValue(fullOtherName) + "']")[0];
            setValidationValues(options, "propertiesvalidation", element);

        });

    jQuery.validator.addMethod("propertiesvalidation", function (value, element, param) {
        var objValType = $(param);
        if (objValType.val() === 'MM') {
            var inputvalue = parseFloat(value);
            var minval = parseFloat(GetValueOfProperty(param, "MinVal"));
            var maxval = parseFloat(GetValueOfProperty(param, "MaxVal"));
            if (inputvalue < minval || inputvalue > maxval)
                return false;
        }
        else if (objValType.val() === 'LEN') {
            var inputvalue = value.length;
            var minlength = parseFloat(GetValueOfProperty(param, "MinLength"));
            var maxlength = parseFloat(GetValueOfProperty(param, "MaxLength"));
            if (inputvalue < minlength || inputvalue > maxlength)
                return false;
        }

        return true;
    });

    jQuery.validator.unobtrusive.adapters.add("greaterthan", ['property'], function (options) {
        options.rules["greaterthan"] = options.params.property;
        options.messages["greaterthan"] = options.message
    });

    jQuery.validator.addMethod("greaterthan", function (value, element, param) {
        var inputvalue = $('input[id="' + element.id + '"]').datepicker().val();
        console.log(element.id);
        console.log(inputvalue);
        var comparevalue = $('input[name="' + param + '"]').val();
        console.log(comparevalue);
        if (Date.parse(inputvalue) < Date.parse(comparevalue)) {
            return false;
        }
        return true;
    });
}(jQuery));



function getModelPrefix(fieldName) {
    return fieldName.substr(0, fieldName.lastIndexOf(".") + 1);
}
function appendModelPrefix(value, prefix) {
    if (value.indexOf("*.") === 0) {
        value = value.replace("*.", prefix);
        return value;
    }
}

function escapeAttributeValue(value) {
    // As mentioned on http://api.jquery.com/category/selectors/
    return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, "\\$1");
}
function setValidationValues(options, ruleName, value) {
    options.rules[ruleName] = value;
    if (options.message) {
        options.messages[ruleName] = options.message;
    }
}

function GetValueOfProperty(value, property) {
    var target = value.name.substr(0, value.name.lastIndexOf(".") + 1);
    target = target + property;
    var returnvalue = $("input[name ='" + target + "']").val();
    return returnvalue;
}


