﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models
{
    public class UserModel
    {
        public string UserId { get; set; }
        public string Acl { get; set; }
        public long AutoAuthorized { get; set; }
        public string DeptId { get; set; }
        public long Effective { get; set; }
        public long Expiry { get; set; }
        public long LastLogin { get; set; }
        public long LastSysTranId { get; set; }
        public string LdapId { get; set; }
        public long MaxConnections { get; set; }
        public string NodeId { get; set; }
        public string PassCode { get; set; }
        public long Sid { get; set; }
        public string Status { get; set; }
        public long TryCount { get; set; }
        public long UpdSeq { get; set; }
        public long UserLevel { get; set; }
        public string UserName { get; set; }
    }
}