﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace los_controller.Models
{
    public class LogIn
    {
        public string Username { get; set; }

        [DataType(DataType.Password)]
        public string password { get; set; }
    }
}