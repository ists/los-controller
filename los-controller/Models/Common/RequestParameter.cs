﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models.Common
{
    public class RequestParameter
    {
        public string Name { get; set; }

        public object obj { get; set; }
    }
}