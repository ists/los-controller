﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models
{
    public class SysRight
    {
        public long Sid { get; set; }
        public string Name { get; set; }
        public los_controller.Utility.EnumUtil.RightEnum Type { get; set; }
        public string Acl { get; set; }
    }
}