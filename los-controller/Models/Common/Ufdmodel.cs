﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using los_controller.Validation;

namespace los_controller.Models.Common
{
    public class Ufdmodel
    {
        public ulong UdfId { get; set; }
        [PropertyValueValidation("ValType", ErrorMessage = "Error Occurs")]
        public string UdfValue { get; set; }

        public string FieldName { get; set; }
        public string FieldType { get; set; }

        public string ValType { get; set; }

        public float MinVal { get; set; }

        public float MaxVal { get; set; }

        public int MinLength { get; set; }

        public int MaxLength { get; set; }
    }
}