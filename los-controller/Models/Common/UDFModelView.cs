﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using los_controller.Utility;

namespace los_controller.Models
{
    public class UdtmFields
    {
        public ulong UdfId { get; set; }
        public string FieldName { get; set; }

        public string FieldDescription { get; set; }

        public string FieldType { get; set; }

        public string ValType { get; set; }

        public string Mandatory { get; set; }

        public bool MandatoryBoolean
        {
            get
            {
                if (!string.IsNullOrEmpty(Mandatory) && Mandatory.Equals(EnumUtil.Allow.Y.ToString()))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    Mandatory = EnumUtil.Allow.Y.ToString();
                else
                    Mandatory = EnumUtil.Allow.N.ToString();
            }
        }

        public float MinVal { get; set; }

        public float MaxVal { get; set; }

        public int MinLength { get; set; }

        public int MaxLength { get; set; }
    }

}