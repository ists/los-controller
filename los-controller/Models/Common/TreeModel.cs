﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models
{
    public class TreeModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string ParentId { get; set; }
    }
}