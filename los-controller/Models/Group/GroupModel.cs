﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models
{
    public class GroupModel
    {
        public string GroupId { get; set; }
        public string Acl { get; set; }
        public string GroupName { get; set; }
        public long LastSysTranId { get; set; }
        public string LdapId { get; set; }
        public long Sid { get; set; }
        public long UpdSeq { get; set; }
    }
}