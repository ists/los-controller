﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace los_controller.Models
{
    public class CustomerCategoryViewModel
    {
        public string CustCatID { get; set; }
        public string CustCatName { get; set; }
        public string ParentID { get; set; }
        public bool InheritConditionBoolean
        {
            get;
            set;
        }
        public bool InheritDocumentBoolean
        {
            get;
            set;
        }
        public bool InheritMenuAclBoolean
        {
            get;
            set;
        }
        public bool InheritParentAclBoolean
        {
            get;
            set;
        }
        public string TranAction { get; set; }
        public string AuditOperation { get; set; }
        public string CustCatAudID { get; set; }
        public string SysTranID { get; set; }
        public string Pid { get; set; }
        public List<SysRight> CustCatRight { get; set; }
    }
}