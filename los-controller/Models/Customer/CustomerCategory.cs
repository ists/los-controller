﻿using los_controller.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Models
{
    public class CustomerCategory
    {
        public string CustCatID { get; set; }
        public string CustCatName { get; set; }
        public string ParentID { get; set; }
        public long InheritCondition { get; set; }
        public bool InheritConditionBoolean
        {
            get
            {
                if (InheritCondition.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritCondition = (long)EnumUtil.Allow.Y;
                else
                    InheritCondition = (long)EnumUtil.Allow.N;
            }
        }
        public long InheritDocument { get; set; }
        public bool InheritDocumentBoolean
        {
            get
            {
                if (InheritDocument.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritDocument = (long)EnumUtil.Allow.Y;
                else
                    InheritDocument = (long)EnumUtil.Allow.N;
            }
        }
        public long InheritMenuAcl { get; set; }
        public bool InheritMenuAclBoolean
        {
            get
            {
                if (InheritMenuAcl.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritMenuAcl = (long)EnumUtil.Allow.Y;
                else
                    InheritMenuAcl = (long)EnumUtil.Allow.N;
            }
        }
        public long InheritParentAcl { get; set; }
        public bool InheritParentAclBoolean
        {
            get
            {
                if (InheritParentAcl.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritParentAcl = (long)EnumUtil.Allow.Y;
                else
                    InheritParentAcl = (long)EnumUtil.Allow.N;
            }
        }
        public string TranAction { get; set; }
        public string AuditOperation { get; set; }
        public string CustCatAudID { get; set; }
        public string SysTranID { get; set; }
        public string Pid { get; set; }
        public List<SysRight> CustCatRight { get; set; }
    }
}
