﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using los_controller.Utility;
using los_controller.Validation;
using System.Web.Mvc;


namespace los_controller.Models
{
    public class ProductViewModel
    {

        [Display(Name = "Product id")]
        [Required(ErrorMessage = "Product id is required")]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Offer ProductId has min length of 4 and max length of 100.")]
        public string ProductId { get; set; }

        public string Pid { get; set; }

        [Display(Name = "Product name")]
        [Required(ErrorMessage = "Product name is required")]
        [StringLength(100, MinimumLength = 4, ErrorMessage = "Offer name has min length of 4 and max length of 100.")]
        public string ProductName { get; set; }

        [Display(Name = "Product slogan")]
        [Required]
        [StringLength(100, MinimumLength = 4, ErrorMessage = "Offer slogan has min length of 4 and max length of 100.")]
        public string ProductSlogan { get; set; }

        [Display(Name = "Parent id")]
        [Required]
        public string ParentId { get; set; }

        [Display(Name = "Product owner")]
        [Required]
        public ulong ProductOwner { get; set; }

        [Display(Name = "Allow sub-product")]
        public bool AllowSubBoolean
        {
            get;
            set;

        }

        [Display(Name = "Allow credit contract")]
        public bool AllowContBoolean
        {

            get;
            set;
        }

        [Display(Name = "Inherit parent’s condition")]
        public bool InheritConditionBoolean
        {
            get;
            set;
        }

        [Display(Name = "Inherit parent’s document")]
        public bool InheritDocumentBoolean
        {
            get;
            set;
        }

        [Display(Name = "Linked CBS product ")]
        public string LinkCbs { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EffectiveDate
        {

            get;
            set;

        }
  
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DateGreater(CompareToPropertyName = "EffectiveDate" , ErrorMessage = "Expiry must greater than Effective")]
        public DateTime ExpiryDate
        {
            get;
            set;

        }

        [Display(Name = "Inherit menu ACL")]
        public bool InheritMenuAclBoolean
        {
            get;
            set;
        }


        [Display(Name = "Inherit parent ACL")]
        public bool InheritParentAclBoolean
        {
            get;
            set;
        }

        public string ProductAudId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string ApprovalStatus { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string AuditOperation { get; set; }

        public int UpdSeq { get; set; }
        public string TranAction { get; set; }

        public string SysTranId { get; set; }
        public List<ProductPropsViewModel> ProductProps { get; set; }

        public List<SysRight> ProductRights { get; set; }

        public ProductViewModel()
        {
            this.EffectiveDate = DateTime.Now;
            this.ExpiryDate = DateTime.Now;
        }
       

    }

    public class ProductPropsViewModel
    {
        public ulong UdfId { get; set; }
        [PropertyValueValidation("ValType", ErrorMessage = "Error Occurs")]
        public string UdfValue { get; set; }

        public string FieldName { get; set; }
        public string FieldType { get; set; }

        public string ValType { get; set; }

        public float MinVal { get; set; }

        public float MaxVal { get; set; }

        public int MinLength { get; set; }

        public int MaxLength { get; set; }
    }

}