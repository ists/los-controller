﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using los_controller.Utility;
using los_controller.Infrastructure.Utility;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace los_controller.Models
{

    public class ProductLite
    {
        public string ProductId { get; set; }
        public string ParentId { get; set; }
        public string ProductName { get; set; }
    }

    public class Product
    {

        public string ProductId { get; set; }

        public string Pid { get; set; }

        public string ProductName { get; set; }
        public string ProductSlogan { get; set; }

        public string ParentId { get; set; }

        public ulong ProductOwner { get; set; }


        public long AllowSub { get; set; }

        public bool AllowSubBoolean
        {
            get
            {
                if (AllowSub.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    AllowSub = (long)EnumUtil.Allow.Y;
                else
                    AllowSub = (long)EnumUtil.Allow.N;
            }

        }

        public long AllowCont { get; set; }

        public bool AllowContBoolean
        {

            get
            {
                if (AllowCont.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    AllowCont = (long)EnumUtil.Allow.Y;
                else
                    AllowCont = (long)EnumUtil.Allow.N;
            }
        }

        public long InheritCondition { get; set; }

        public bool InheritConditionBoolean
        {
            get
            {
                if (AllowCont.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    AllowCont = (long)EnumUtil.Allow.Y;
                else
                    AllowCont = (long)EnumUtil.Allow.N;
            }
        }

        public long InheritDocument { get; set; }

        public bool InheritDocumentBoolean
        {
            get
            {
                if (InheritDocument.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritDocument = (long)EnumUtil.Allow.Y;
                else
                    InheritDocument = (long)EnumUtil.Allow.N;
            }
        }


        public string LinkCbs { get; set; }

        public long Effective
        {
            get;
            set;

        }
        public DateTime EffectiveDate
        {

            get
            {
                if (string.IsNullOrEmpty(Effective.ToString()))
                    return DateTime.Now;
                else
                {
                    return UtilityClass.ToDateTime(DateTime.UtcNow,Effective);
                }
            }
            set
            {

                Effective = UtilityClass.UnixTimestampFromDateTime(value);
            }
        }

        public long Expiry { get; set; }
        public DateTime ExpiryDate
        {
            get
            {
                if (string.IsNullOrEmpty(Expiry.ToString()))
                    return DateTime.Now;
                else
                {
                    return UtilityClass.ToDateTime(DateTime.UtcNow,Expiry);
                }
            }
            set
            {
                Expiry = UtilityClass.UnixTimestampFromDateTime(value);
            }

        }

        public long InheritMenuAcl { get; set; }

        public bool InheritMenuAclBoolean
        {
            get
            {
                if (InheritMenuAcl.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritMenuAcl = (long)EnumUtil.Allow.Y;
                else
                    InheritMenuAcl = (long)EnumUtil.Allow.N;
            }
        }

        public long InheritParentAcl { get; set; }
        public bool InheritParentAclBoolean
        {
            get
            {
                if (InheritParentAcl.Equals((long)EnumUtil.Allow.Y))
                    return true;
                else
                    return false;
            }

            set
            {
                if (value)
                    InheritParentAcl = (long)EnumUtil.Allow.Y;
                else
                    InheritParentAcl = (long)EnumUtil.Allow.N;
            }
        }

        public int UpdSeq { get; set; }

        public string ProductAudId { get; set; }

        public string AuditOperation { get; set; }

        public string TranAction { get; set; }

        public string SysTranId { get; set; }
        public List<ProductProps> ProductProps { get; set; }
        public List<SysRight> ProductRights { get; set; }
    }

    public class ProductProps
    {
        public string ProductId { get; set; }

        public ulong UdfId { get; set; }

        public string UdfValue { get; set; }

        public UdtmFields udtmField { get; set; }
    }

    public class ProductAudLite
    {
        public string productAudId { get; set; }
        public string productName { get; set; }
        public string Creator { get; set; }
        public string userId { get; set; }

        public long auditDate
        {
            get;
            set;
        }

        public DateTime AuditDateFormat
        {
            get
            {
                if (string.IsNullOrEmpty(auditDate.ToString()))
                    return DateTime.MinValue;
                else
                {
                    return UtilityClass.ToDateTime(DateTime.UtcNow,auditDate);
                }
            }
        }
        public string approvalStatus { get; set; }
    }
}