﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;



namespace los_controller.Infrastructure.RestConnect
{
    public class RestCore : RestClient
    {

        private string url = "http://devserver:9000/";

        public RestCore()
        {
            this.BaseUrl = url;
        }
    }

    public class RestCoreFake : RestClient
    {
        private string url = "http://devserver:8080/";

        public RestCoreFake()
        {
            this.BaseUrl = url;
        }
    }
}