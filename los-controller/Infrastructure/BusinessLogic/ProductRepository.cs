﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using los_controller.Infrastructure.RestConnect;
using los_controller.Infrastructure.Utility;
using RestSharp;
using los_controller.Models;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Script.Serialization;
using System.Collections;
using los_controller.Utility;
using AutoMapper;

namespace los_controller.Infrastructure.BusinessLogic
{
    public class ProductRepository
    {
        private const string PRODUCTID = null;
        private static RestCoreFake clientfake = new RestCoreFake();
        private static RestCore client = new RestCore();

        public static List<TreeModel> GetProductTree()
        {

            RestRequest request = new RestRequest("product/getproducttree/1/demo.inputuser3", Method.GET);
            RestResponse<List<TreeModel>> response = (RestResponse<List<TreeModel>>)client.Execute<List<TreeModel>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static Product GetProductDetail(string id)
        {
            RestRequest request = new RestRequest(string.Format("product/getproductdetail/{0}", id), Method.GET);
            RestResponse<Product> response = (RestResponse<Product>)client.Execute<Product>(request);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }

        public static List<ProductProps> GetProductProps(string productId) {
            string requestUrl = "";
            if(string.IsNullOrEmpty(productId))
                requestUrl = "product/getproductdetailprops";
            else
                requestUrl= string.Format("product/getproductdetailprops/{0}", productId);
            RestRequest request = new RestRequest(requestUrl, Method.GET);
            RestResponse<List<ProductProps>> response = (RestResponse<List<ProductProps>>)client.Execute<List<ProductProps>>(request);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static List<SysRight> GetProductRights(string productId)
        {
            productId = string.IsNullOrEmpty(productId) ? PRODUCTID : productId;
            RestRequest request = new RestRequest(string.Format("product/getproductdetailright/{0}", productId), Method.GET);
            RestResponse<List<SysRight>> response = (RestResponse<List<SysRight>>)client.Execute<List<SysRight>>(request);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static ButtonModel GetPermittance(string productID)
        {
            RestRequest request = new RestRequest(string.Format("product/getproductright/1/{0}/demo.inputuser3", productID), Method.GET);
            RestResponse response = (RestResponse)client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            ButtonModel Buttonbar = new ButtonModel { rightName = response.Content };
            return Buttonbar;
         
        }

        public static void DeleteProduct(string id)
        {
            RestRequest request = new RestRequest(string.Format("product/{0}",id), Method.DELETE);
            RestResponse response = (RestResponse)client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException(response.ErrorMessage);
        }

        public static void SaveProduct(Product model)
        {
           
            RestRequest request = new RestRequest("/product/saveproduct", Method.POST);
            request.AddHeader("Accept", "application/json");

            request.AddParameter("application/json", UtilityClass.newtonJsonSerialize(model), ParameterType.RequestBody);

            RestResponse response = (RestResponse)client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException(response.ErrorMessage);
        }

        public static List<ProductAudLite> GetProductAudList()
        {

            RestRequest request = new RestRequest("product/getproductaudforapproval", Method.GET);
            RestResponse<List<ProductAudLite>> response = (RestResponse<List<ProductAudLite>>)client.Execute<List<ProductAudLite>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static Product GetProductAudDetail(string productAudID)
        {
            RestRequest request = new RestRequest(string.Format("productAud/{0}", productAudID), Method.GET);
            RestResponse<Product> response = (RestResponse<Product>)clientfake.Execute<Product>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }

        public static List<ProductProps> GetProductAudProps(string productAudID)
        {
            string requestUrl = string.Format("product/getproductauddetailprops/{0}", productAudID);
            RestRequest request = new RestRequest(requestUrl, Method.GET);
            RestResponse<List<ProductProps>> response = (RestResponse<List<ProductProps>>)client.Execute<List<ProductProps>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }

        public static List<SysRight> GetProductAudRight(string productAudID)
        {
            RestRequest request = new RestRequest(string.Format("product/getproductauddetailright/{0}", productAudID), Method.GET);
            RestResponse<List<SysRight>> response = (RestResponse<List<SysRight>>)client.Execute<List<SysRight>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }


        public static HttpStatusCode ApproveProduct(string ProductAudId)
        {
            RestRequest request = new RestRequest("product/approveproductaud", Method.POST);
            //request.AddHeader("Content-Type", "application/json");
            //request.AddParameter("productAudId", long.Parse(ProductAudId), ParameterType.RequestBody);

            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/json", UtilityClass.newtonJsonSerialize(new { productAudId = long.Parse(ProductAudId) }), ParameterType.RequestBody);
            //request.AddParameter("productAudId", long.Parse(ProductAudId), ParameterType.RequestBody);

            RestResponse response = (RestResponse)client.Execute(request);
            return response.StatusCode;
        }

        public static HttpStatusCode RejectProduct(string ProductAudId)
        {
            RestRequest request = new RestRequest("productAud/Reject", Method.POST);
            request.AddParameter("ProductAudId",UtilityClass.newtonJsonSerialize(ProductAudId),ParameterType.GetOrPost);
            RestResponse response = (RestResponse)clientfake.Execute(request);
            return response.StatusCode;
        }


        public static List<Product> GetProductListForAddParent()
        {
            RestRequest request = new RestRequest("product/getproductlistforaddparent", Method.GET);
            RestResponse<List<Product>> response = (RestResponse<List<Product>>)client.Execute<List<Product>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }
    }
}