﻿using los_controller.Infrastructure.RestConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using los_controller.Models;
using RestSharp;
using System.Net;
using los_controller.Utility;

namespace los_controller.Infrastructure.BusinessLogic
{
    public class CustomerCategoryRepository
    {
        private static RestCore client = new RestCore();
        public static List<TreeModel> GetCustomerCategoryTree ()
        {
            RestRequest request = new RestRequest("product", Method.GET);
            RestResponse<List<TreeModel>> response = (RestResponse<List<TreeModel>>)client.Execute<List<TreeModel>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static CustomerCategory GetCustomerCategoryDetail()
        {
            RestRequest request = new RestRequest("product", Method.GET);
            RestResponse<CustomerCategory> response = (RestResponse<CustomerCategory>)client.Execute<CustomerCategory>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");

            return response.Data;
        }

        public static void SaveCustomerCategory()
        {

        }

        public static string DeleteCustomerCategory(string custcatid)
        {
            RestRequest request = new RestRequest("customercategory/delete");
            request.AddParameter("CustCatID", custcatid, ParameterType.RequestBody);
            RestResponse response = (RestResponse)client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.StatusDescription;
        }

        public static void GetCustomerCategoryAuditList()
        {

        }

        public static void ApproveCustomerCategory()
        {

        }

        public static void RejectCustomerCategory()
        { }


    }
}