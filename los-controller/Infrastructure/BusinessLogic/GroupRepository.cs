﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using los_controller.Infrastructure;
using los_controller.Models;
using RestSharp;
using System.Net;
using los_controller.Utility;
using los_controller.Infrastructure.RestConnect;

namespace los_controller.Infrastructure.BusinessLogic
{
    public class GroupRepository
    {
        static RestCore client = new RestCore();

        public static List<GroupModel>  GetGroupList()
        {
            RestRequest request = new RestRequest("group/getgrouplistforaddright", Method.GET);
            RestResponse<List<GroupModel>> response = (RestResponse<List<GroupModel>>)client.Execute<List<GroupModel>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }
    }
}