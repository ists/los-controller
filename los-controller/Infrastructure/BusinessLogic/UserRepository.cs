﻿using los_controller.Models;
using los_controller.Utility;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace los_controller.Infrastructure.BusinessLogic
{
    public class UserRepository
    {
        private static RestConnect.RestCore client = new RestConnect.RestCore();

        public static List<UserModel> GetUserList()
        {
            RestRequest request = new RestRequest("user/getuserlistforaddright", Method.GET);
            RestResponse<List<UserModel>> response = (RestResponse<List<UserModel>>)client.Execute<List<UserModel>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new RestErrorException("Error when contact with RESTful API");
            return response.Data;
        }
    }
}