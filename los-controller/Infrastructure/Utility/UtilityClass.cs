﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Globalization;

namespace los_controller.Infrastructure.Utility
{
  

    public enum fieldGroupID { Product, Group };
    public class UtilityClass
    {
        public static string jsonSerialize(object obj)
        {
            var jsonconvert = new JavaScriptSerializer();
            var result = jsonconvert.Serialize(obj);
            return result;
        }

        public static string newtonJsonSerialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }

        public static string getMessageError(string content)
        {
            var jsonconvert = new JavaScriptSerializer();
            Dictionary<string, object> result = (Dictionary<string, object>)jsonconvert.DeserializeObject(content);
            return result["message"].ToString();

        }


        public static DateTime ToDateTime(DateTime target, long timestamp)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);

            return dateTime.AddSeconds((timestamp/1000));
        }

        public static long UnixTimestampFromDateTime(DateTime target)
        {
            var date = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
            var unixTimestamp = System.Convert.ToInt64((target - date).TotalMilliseconds);

            return unixTimestamp;
        }


        
    }


}