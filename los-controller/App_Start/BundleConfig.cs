﻿using System.Web;
using System.Web.Optimization;

namespace los_controller
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/module.js",
                        "~/Scripts/jquery-ACL.js",
                        "~/Scripts/svgcheckbox.js",
                        "~/Scripts/datepicker.js"
                        
                        ));

            bundles.Add(new ScriptBundle("~/Scripts/jlinq").Include(
                        "~/Scripts/jlinq.js",
                        "~/Scripts/jlinq.jquery.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/product/CustomValidation.js"));
       

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquerylogin").Include(
                        "~/Scripts/logIn.jquery.js",
                        "~/Scripts/jquery.query-2.1.7.js",
                        "~/Scripts/rainbows.js"));


            bundles.Add(new ScriptBundle("~/bundles/underscore").Include(
                        "~/Scripts/underscore.js"));


            bundles.Add(new ScriptBundle("~/bundles/tree").Include(
                        "~/Scripts/tree.js"));

            bundles.Add(new ScriptBundle("~/bundles/product").Include(
                        "~/Scripts/product/module.product.js",
                        "~/Scripts/product/product.js"));

            bundles.Add(new ScriptBundle("~/bundles/ProductAction").Include(
                        "~/Scripts/product/ProductAction.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Site.css",
                      "~/Content/css/import/svgcheckbox.css",
                      "~/Content/css/jquery-ui/all.css"));


            bundles.Add(new StyleBundle("~/Style/Product").Include(
                        "~/Content/css/product.css"
                ));

            bundles.Add(new StyleBundle("~/Style/CustomerCategory").Include(
                        "~/Content/css/customerCategory.css"
                ));

            bundles.Add(new StyleBundle("~/Script/CustomerCategory").Include(
                        "~/Scripts/Customer/module.customer.category.js",
                        "~/Scripts/Customer/customerCategory.js"
                ));

            bundles.Add(new ScriptBundle("~/Script/Dropdown").Include(
                            "~/Scripts/jquery.dd.min.js"
                ));
        }
    }
}
