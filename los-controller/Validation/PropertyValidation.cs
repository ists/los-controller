﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using los_controller.Utility;
using los_controller.Models;


namespace los_controller.Validation
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple=true,Inherited=false)]
    public class PropertyValueValidation : ValidationAttribute,IClientValidatable
    {

        private string Val_type;
        public PropertyValueValidation(string Valtype="") : base("Error")
        {
            Val_type = Valtype;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
   
            string Validtype = (string)GetPropertyValue.GetValueOfProperty(validationContext, Val_type);
            if (Validtype == EnumUtil.Val_Type.MM.ToString())
            {
                float minvalue = (float)GetPropertyValue.GetValueOfProperty(validationContext, "MinVal");
                float maxvalue = (float)GetPropertyValue.GetValueOfProperty(validationContext, "MaxVal");
                float thisvalue = float.Parse(value.ToString());
                if (thisvalue < minvalue || thisvalue > maxvalue)
                {
                    string errormessage = string.Format("Validation : {0} < Value < {1} ", minvalue, maxvalue);
                    return new ValidationResult(errormessage);
                }
            }
            else if (Validtype == EnumUtil.Val_Type.LEN.ToString())
            {
                int minlength = (int)GetPropertyValue.GetValueOfProperty(validationContext, "MinLength");
                int maxlength = (int)GetPropertyValue.GetValueOfProperty(validationContext, "MaxLength");
                int thislength = value.ToString().Length;
                if(thislength < minlength || thislength > maxlength)
                {
                     string errormessage = string.Format("Validation : {0} < Length < {1} ", minlength, maxlength);
                     return new ValidationResult(errormessage);
                }
            }
            return ValidationResult.Success;    
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {

            ModelClientValidationRule mcr = new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "propertiesvalidation"
            };
            mcr.ValidationParameters.Add("other", "*." + Val_type);
            yield return mcr;
            
        }
    }

    
}