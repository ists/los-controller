﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using los_controller.Utility;
using los_controller.Models;
namespace los_controller.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class DateGreater : ValidationAttribute, IClientValidatable
    {
        public string CompareToPropertyName { get; set; }

        public string Err { get; set; }

        public DateGreater()
            : base()
        {

        }

        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var Comparewith = GetPropertyValue.GetValueOfProperty(validationContext, CompareToPropertyName);
            if (Comparewith is DateTime && value is DateTime)
            {
                if ((DateTime)value < (DateTime)Comparewith)
                {
                    return new ValidationResult(ErrorMessage);
                }
            }
            return ValidationResult.Success;
        }



        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule modelclient = new ModelClientValidationRule { 
                ErrorMessage=ErrorMessage,
                ValidationType="greaterthan"
            };
            modelclient.ValidationParameters.Add("property", CompareToPropertyName);
            yield return modelclient;
        }
    }
}