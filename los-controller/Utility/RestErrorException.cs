﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace los_controller.Utility
{
    public class RestErrorException: InvalidOperationException
    {
        public RestErrorException()
        {

        }

        public RestErrorException(string message): base(message)
        {

        }

        public RestErrorException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}