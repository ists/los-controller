﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace los_controller.Utility
{
    public class GetPropertyValue
    {
        public static object GetValueOfProperty(ValidationContext validationContext, string Property)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(Property);
            var ReturnValue = propertyInfo.GetValue(validationContext.ObjectInstance, null);
            return ReturnValue;
        }
    }
}