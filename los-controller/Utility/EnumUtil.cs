﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using RestSharp;

namespace los_controller.Utility
{
    public static class EnumUtil
    {
        public static string GetDescription<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public enum Allow
        {
            N,
            Y
        }
        public enum RightEnum
        {
            User = 1,
            Group = 2
        }

        public enum AclEnum
        {
            [Description("View")]
            V,
            [Description("Input")]
            I,
            [Description("Update")]
            U,
            [Description("Delete")]
            D,
            [Description("Change Parent")]
            E,
            [Description("Approve")]
            A,
            [Description("Create sub")]
            S,
            [Description("Execute")]
            X,
            [Description("Print")]
            P
        }

        public enum RestMethod
        {
           SAVE = Method.POST,
           GET = Method.GET,
           DELETE = Method.DELETE,
           PUT = Method.PUT
        }

       public enum ValueType
       {
           T,D,N
       }

        public enum Val_Type
        {
            LOV,
            MM,
            LEN,
            MAS,
            TAB,
            FUNC,
            NON

        }
    }
}