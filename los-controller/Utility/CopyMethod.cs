﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using los_controller.Models;
using AutoMapper;

namespace los_controller.Utility
{
    public class CopyMethod
    {
        public static ProductViewModel CopyProductModel(Product model)
        {
            Mapper.CreateMap<Product, ProductViewModel>();

            Mapper.CreateMap<ProductProps, ProductPropsViewModel>()
                .ForMember(dest => dest.UdfId, opt => opt.MapFrom(src => src.udtmField.UdfId))
                .ForMember(dest =>dest.FieldName,opt =>opt.MapFrom(src =>src.udtmField.FieldName))
               .ForMember(dest => dest.ValType, opt => opt.MapFrom(src => src.udtmField.ValType))
               .ForMember(dest => dest.MinVal, opt => opt.MapFrom(src => src.udtmField.MinVal))
               .ForMember(dest => dest.MaxVal, opt => opt.MapFrom(src => src.udtmField.MaxVal))
               .ForMember(dest => dest.MinLength, opt => opt.MapFrom(src => src.udtmField.MinLength))
               .ForMember(dest => dest.MaxLength, opt => opt.MapFrom(src => src.udtmField.MaxLength));
            
            ProductViewModel viewmodel = Mapper.Map<Product, ProductViewModel>(model);
            return viewmodel;
        }

       public static List<ProductPropsViewModel> CopyProductPropsModel(List<ProductProps> models)
        {
           Mapper.CreateMap<ProductProps, ProductPropsViewModel>()
               .ForMember(dest => dest.UdfId, opt => opt.MapFrom(src => src.udtmField.UdfId))
                .ForMember(dest => dest.FieldName, opt => opt.MapFrom(src => src.udtmField.FieldName))
               .ForMember(dest => dest.ValType, opt => opt.MapFrom(src => src.udtmField.ValType))
               .ForMember(dest => dest.MinVal, opt => opt.MapFrom(src => src.udtmField.MinVal))
               .ForMember(dest => dest.MaxVal, opt => opt.MapFrom(src => src.udtmField.MaxVal))
               .ForMember(dest => dest.MinLength, opt => opt.MapFrom(src => src.udtmField.MinLength))
               .ForMember(dest => dest.MaxLength, opt => opt.MapFrom(src => src.udtmField.MaxLength));
            List<ProductPropsViewModel> viewmodel = Mapper.Map<List<ProductProps>, List<ProductPropsViewModel>>(models);
            return viewmodel;
        }
        
    }
}