﻿using los_controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace los_controller.Controllers
{
    public class BaseController : Controller
    {
       
        protected override void OnException(ExceptionContext filterContext)
        {
            //If the exception is already handled we do nothing
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            else
            {
                //Determine the return type of the action
                string actionName = filterContext.RouteData.Values["action"].ToString();
                Type controllerType = filterContext.Controller.GetType();
                var method = controllerType.GetMethod(actionName);
                var returnType = method.ReturnType;

                ErrorModel errorModel = new ErrorModel()
                {
                    ErrorCode = filterContext.Exception.Message,
                    ErrorMessage = filterContext.Exception.StackTrace
                };

                //If the action that generated the exception returns JSON
                if (returnType.Equals(typeof(JsonResult)) || returnType.Equals(typeof(EmptyResult)) || returnType.Equals(typeof(HttpStatusCodeResult)))
                {
                    filterContext.HttpContext.Response.StatusCode = 500;
                    filterContext.Result = new JsonResult()
                    {
                        Data = errorModel,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }

                //If the action that generated the exception returns a view
                //Thank you Sumesh for the comment
                else if (returnType.Equals(typeof(ActionResult))
                     || (returnType).IsSubclassOf(typeof(ActionResult)))
                {
                    filterContext.Result = new ViewResult()
                    {
                        ViewName = "~/Views/Error/GlobalError.cshtml",
                        ViewData = new ViewDataDictionary<ErrorModel>(errorModel),
                    };
                }
            }

            //Make sure that we mark the exception as handled
            filterContext.ExceptionHandled = true;
        }
    }
}