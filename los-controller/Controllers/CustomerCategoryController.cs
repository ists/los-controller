﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using los_controller.Models;

namespace los_controller.Controllers
{
    
    public class CustomerCategoryController : Controller
    {
        //
        // GET: /CustomerCategory/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCustCatTree()
        {
            List<TreeModel> treelist = new List<TreeModel> {
                new TreeModel { Id="Ca1", Name="Cattest2" , ParentId="Ca"},
                new TreeModel { Id="Ca", Name = "Cattest1", ParentId=  null },
                new TreeModel { Id="Ca2", Name="Cattest3" , ParentId= "Ca"}
            };
            return Json(treelist,JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult GetCustCatDetail(string CustCatID)
        {
            return PartialView("CustomerCategoryDetail");
        }

        [HttpGet]
        public PartialViewResult NewCustomerCatgory()
        {
            CustomerCategoryViewModel model = new CustomerCategoryViewModel();
            model.AuditOperation = "I";
            model.TranAction = "I";
            return PartialView("CustomerCategoryEditView", model);
        }

        [HttpGet]
        public PartialViewResult EditCustomerCategory(string CustCatID)
        {
            CustomerCategory model = new CustomerCategory();
            model.AuditOperation = "U";
            model.TranAction = "I";
            return PartialView("CustomerCategoryEditView",model);
        }

        public JsonResult SaveCustomerCategory(CustomerCategoryViewModel model)
        {
            
            return Json("True");
        }

        public JsonResult DeleteCustomerCategory(string CustCatID)
        {
            return Json("True");
        }

        public ViewResult GetAudList()
        {
            return View();
        }

        public PartialViewResult GetAudDetail(string CustcatAudId)
        {
            return PartialView("");
        }

        public JsonResult Approve(string CustcatAudId)
        {
            return Json("");
        }

        public JsonResult Reject(string CustcatAudId)
        {
            return Json("");
        }
	}
}