﻿using los_controller.Infrastructure.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace los_controller.Controllers
{
    public  class GroupController : Controller
    {
        
        //
        // GET: /Group/
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetGroupList()
        {

            return Json(GroupRepository.GetGroupList(), JsonRequestBehavior.AllowGet);
        }
	}
}