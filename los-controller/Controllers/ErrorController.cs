﻿using los_controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace los_controller.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult GlobalError()
        {
            return View(new ErrorModel());
        }
	}
}