﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using los_controller.Infrastructure.BusinessLogic;

namespace los_controller.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetUserList()
        {
            var data = UserRepository.GetUserList();
            return Json(data,JsonRequestBehavior.AllowGet);
        }

        
	}
}