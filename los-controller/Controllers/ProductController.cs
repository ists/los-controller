﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using los_controller.Models;
using los_controller.Infrastructure.BusinessLogic;
using RestSharp;
using System.Net;
using los_controller.Infrastructure.Utility;
using System.Web.Script.Serialization;
using los_controller.Validation;
using AutoMapper;
using los_controller.Utility;
using DevExpress.Web.Mvc;

namespace los_controller.Controllers
{
    [Authorize]
    public class ProductController : BaseController
    {


        //
        // GET: /Product/
        public ViewResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetProductTree()
        {
            List<TreeModel> listresult = ProductRepository.GetProductTree();
            return Json(listresult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult GetProductDetail(string productId)
        {
            ViewData["ButtonBar"] = ProductRepository.GetPermittance(productId);
            Product product = ProductRepository.GetProductDetail(productId);
            product.ProductProps = ProductRepository.GetProductProps(productId);
            product.ProductRights = ProductRepository.GetProductRights(productId);
            ProductViewModel productview = CopyMethod.CopyProductModel(product);
            return PartialView("ProductDetail", productview);
        }

        public PartialViewResult NewProduct(string ProductId)
        {
            List<Product> listresult = ProductRepository.GetProductListForAddParent();
            ViewBag.ParentId = new SelectList(listresult, "ProductId", "ProductName",ProductId);
            ProductViewModel productview = new ProductViewModel();
            productview.ParentId = ProductId;
            productview.AuditOperation = "I";
            productview.TranAction = "I";
            productview.ProductProps = CopyMethod.CopyProductPropsModel(ProductRepository.GetProductProps(null));
            ViewBag.UserList = new SelectList(UserRepository.GetUserList(), "Sid", "UserName");
            return PartialView("ProductDetailEdit", productview);
        }

        public PartialViewResult EditProduct(string ProductId)
        {
           
            Product product = ProductRepository.GetProductDetail(ProductId);
            product.ProductProps = ProductRepository.GetProductProps(ProductId);
            product.ProductRights = ProductRepository.GetProductRights(ProductId);
            product.AuditOperation = "U";
            product.TranAction = "I";
            List<Product> listresult = ProductRepository.GetProductListForAddParent().Where(s =>s.ProductId != ProductId).ToList();
            ViewBag.ParentId = new SelectList(listresult, "ProductId", "ProductName", product.ParentId);
            ViewBag.ListUser = new SelectList(UserRepository.GetUserList(), "Sid", "UserName");
            ViewBag.ListGroup = new SelectList(GroupRepository.GetGroupList(), "Sid", "GroupName");
            ProductViewModel model = CopyMethod.CopyProductModel(product);
            return PartialView("ProductDetailEdit", model);
        }

        [ChildActionOnly]
        public PartialViewResult RenderSysRight(List<SysRight> models)
        {
            ViewBag.ListUser = new SelectList(UserRepository.GetUserList(), "Sid", "UserName");
            ViewBag.ListGroup = new SelectList(GroupRepository.GetGroupList(), "Sid", "GroupName");
            return PartialView("_EditACL", models);
        }


        [HttpPost]
        public JsonResult SaveProduct(ProductViewModel productview, string Sysright)
        {
            ValidateModel(productview);
            JavaScriptSerializer jsonconvert = new JavaScriptSerializer();
            List<SysRight> listright = jsonconvert.Deserialize<List<SysRight>>(Sysright);
            productview.ProductRights = listright;
            Mapper.CreateMap<ProductViewModel, Product>();
            Mapper.CreateMap<ProductPropsViewModel, ProductProps>();
            Product productmodel = Mapper.Map<ProductViewModel, Product>(productview);
            ProductRepository.SaveProduct(productmodel);
            return Json(productmodel);
        }

        [HttpPost]
        public EmptyResult DeleteProduct(string ProductId)
        {
            ProductRepository.DeleteProduct(ProductId);

            return new EmptyResult();
        }

        public ViewResult GetProductAudList()
        {

            return View("GridViewDevexpress");
        }

        public PartialViewResult getProductAudDetail(string productAudID)
        {
            Product model = ProductRepository.GetProductAudDetail(productAudID);
            model.ProductProps = ProductRepository.GetProductAudProps(productAudID);
            model.ProductRights = ProductRepository.GetProductAudRight(productAudID);
            return PartialView("ApprovalProductDetail", model);
        }

        public JsonResult Approve(string productAudID)
        {
            HttpStatusCode statuscode = ProductRepository.ApproveProduct(productAudID);
            return Json(statuscode, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Reject(string productAudID)
        {
            return Json("true", JsonRequestBehavior.AllowGet);
        }




        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = ProductRepository.GetProductAudList();
            return PartialView("_GridViewPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] los_controller.Models.ProductAudLite item)
        {
            var model = new object[0];
            if (ModelState.IsValid)
            {
                try
                {
                    // Insert here a code to insert the new item in your model
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] los_controller.Models.ProductAudLite item)
        {
            var model = new object[0];
            if (ModelState.IsValid)
            {
                try
                {
                    // Insert here a code to update the item in your model
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String productAudId)
        {
            var model = new object[0];
            if (productAudId != null)
            {
                try
                {
                    // Insert here a code to delete the item from your model
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model);
        }
    }
}