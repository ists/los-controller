﻿(function ($) {
    $.fn.extend({
        eventTree: function (setting, callback) {
            $(this).find("a").click(function (e) {
                $('.active-tree').removeClass('active-tree');
                $(this).addClass('active-tree');


                var id = $(this).parent().data("id");
                // event ajax
                var data = {
                    id: id,
                    name: "xyz"
                };
                callback(data);
                e.stopPropagation();
            });


            $(this).find('#input-seach-tree').keyup(function (e) {
                var textSeach = this.value;
                if (this.value == ' ') {
                    this.value = '';
                } else if (this.value !== '') {
                    $('.listSeachTree').remove();
                    var ul = $('<ul class="listSeachTree"></ul>');
                    $('#treeBody ul').first().find('a').each(function (index, value) {
                        if (value.text.indexOf(textSeach) !== -1) {
                            ul.append($(value).parent().clone().removeClass('listTree listTreeDau').click(function () {
                                $('.active-tree').removeClass('active-tree');
                                $(this).children('a').addClass('active-tree');
                                $('#body-content').html('');
                                //var loading = $().loading();
                                //$('#body-content').append(loading);
                                var data = {
                                    id: $(this).data('id'),
                                    name: "abc"
                                };
                                callback(data);
                                e.stopPropagation();
                            }).each(function (index, item) {
                                return $(item).children('ul').remove();
                            }));
                        }
                    });
                    $('#treeBody').find('ul').hide().end().append(ul);
                } else {
                    $('#treeBody ul').first().find('a').each(function (index, value) {
                        if ($(value).text() === $('.active-tree').text()) {
                            $(value).addClass('active-tree');
                        }
                    });
                    $('.active-tree').parents('#treeBody ul').show();
                    $('.listSeachTree').remove();
                    $('#treeBody > ul').show();
                }

            });

            return this;
        }
    });
})(jQuery);