﻿$(document).ready(function () {

    $('#SaveButton').click(function (e) {
        var Controller = $('#CurentController').data('value');
        var urlSave = '/' + Controller + '/Save';
        console.log(urlSave);
        if ($('#save-form').valid()) {
            $.ajax({
                url: urlSave,
                data: $('#save-form').serialize(),
                type: 'POST',
                success: function (returndata) {
                    window.location = returndata.newurl
                },
                error: function () {

                }
            });
        } else {
            console.log('validate failed');
        }
    });
});