﻿using Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Framework.Controllers
{
    public class SecurityController : Controller
    {
        public ActionResult logIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult logIn(LogInModel model)
        {
            bool result = FormsAuthentication.Authenticate(model.Username, model.password);
            if (result)
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);
                string SessionID = HttpContext.Session.SessionID;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("Error", "Oops!");
            }
            return View(model);
        }

        public ActionResult logOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("logIn");
        }
	}
}