﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.Models;

namespace Framework.Controllers
{
    public class CustomerController : BaseController<Customer>
    {
        static List<Customer> customerList = new List<Customer> { 
             new Customer { Id="KH1", Name="Huy"},
             new Customer { Id="KH2", Name="Thong"},
             new Customer { Id="KH3", Name="Dan"},
             new Customer { Id="KH4", Name="VA"},
        };
        //
        // GET: /Customer/


        public override ActionResult Index()
        {
            ButtonModel button = new ButtonModel { RightName = "I", DestId = null };
            ViewData["ButtonBar"] = button;
            return View(customerList);
        }

        public override ActionResult GetDetail(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "IUD", DestId = destId };
            ViewData["ButtonBar"] = button;
            var model = customerList.SingleOrDefault(x => x.Id == destId);
            return View("CustomerDetailView", model);
        }

        public override ActionResult Insert(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "S", DestId = destId };
            ViewData["ButtonBar"] = button;
            return View("CustomerEditView", null);
        }

        public override ActionResult Update(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "S", DestId = destId };
            ViewData["ButtonBar"] = button;
            var model = customerList.SingleOrDefault(x => x.Id == destId);
            return View("CustomerEditView", model);
        }

        public override ActionResult Delete(string destId)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Save(Customer model)
        {
            List<AuditModel> auditList = new List<AuditModel>();
            AuditModel auditmodel = new AuditModel { Id = model.Id, Module = "Customer", Slogan = model.Name };
            if (Session["AuditList"] == null)
            {
                auditList.Add(auditmodel);
                Session["AuditList"] = auditList;
            }
            else
                ((List<AuditModel>)Session["AuditList"]).Add(auditmodel);
            if (Session["Approvemodel"] == null)
            {
                Dictionary<string, object> dicfake = new Dictionary<string, object>();
                dicfake.Add(model.Id, model);
                Session["Approvemodel"] = dicfake;
            }
            else
                ((Dictionary<string, object>)Session["Approvemodel"]).Add(model.Id, model);
            return Json(new { ok = true, newurl = Url.Action("Index") });
        }

        public override ActionResult GetById(string destId)
        {
            throw new NotImplementedException();
        }
    }
}