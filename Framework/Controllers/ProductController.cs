﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.Models;


namespace Framework.Controllers
{
    public class ProductController : BaseController<Product>
    {
        List<AuditModel> auditList = new List<AuditModel>();

        public static List<Product> productlist = new List<Product> { 
            new Product { Id="TD" , ParentId=null, Slogan="Tin dung chung"},
            new Product { Id="KH",  ParentId="TD", Slogan="Tin dung cho khach hang"},
            new Product { Id="OTO", ParentId="TD", Slogan="Tin dung mua oto"},
            new Product { Id="NHA",  ParentId="KH", Slogan="Tin dung mua nha"}
        };

        public override ActionResult Index()
        {
            ButtonModel button = new ButtonModel { RightName = "I", DestId = null };
            ViewData["ButtonBar"] = button;

            return View(productlist);
        }

        [HttpPost]
        public ActionResult Content()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tree()
        {
            return View();
        }
        public override ActionResult GetDetail(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "IUD", DestId = destId };
            ViewData["ButtonBar"] = button;
            var model = productlist.SingleOrDefault(x => x.Id == destId);
            return View("ProductDetailView", model);
        }

        public override ActionResult Insert(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "S", DestId = destId };
            ViewData["ButtonBar"] = button;
            return View("ProductEditView", null);
        }

        public override ActionResult Update(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "S", DestId = destId };
            ViewData["ButtonBar"] = button;
            var model = productlist.SingleOrDefault(x => x.Id == destId);
            return View("ProductEditView", model);
        }

        public override ActionResult Delete(string destId)
        {
            var remodel = productlist.SingleOrDefault(s => s.Id == destId);
            productlist.Remove(remodel);
            return RedirectToAction("Index");
        }

        public override ActionResult Save(Product model)
        {
            AuditModel auditmodel = new AuditModel { Id = model.Id, Module = "Product", Slogan = model.Slogan };
            if (Session["AuditList"] == null)
            {
                auditList.Add(auditmodel);
                Session["AuditList"] = auditList;
            }
            else
                ((List<AuditModel>)Session["AuditList"]).Add(auditmodel);
            if (Session["Approvemodel"] == null)
            {
                Dictionary<string, object> dicfake = new Dictionary<string, object>();
                dicfake.Add(model.Id, model);
                Session["Approvemodel"] = dicfake;
            }
            else
                ((Dictionary<string, object>)Session["Approvemodel"]).Add(model.Id, model);
            return Json(new { ok = true, newurl = Url.Action("Index") });
        }

        public override ActionResult GetById(string destId)
        {
            ButtonModel button = new ButtonModel { RightName = "A", DestId = destId };
            ViewData["ButtonBar"] = button;
            var model = productlist.SingleOrDefault(x => x.Id == destId);
            return View("ProductDetail", model);
        }

        public override ActionResult Approve(string destId)
        {
            Product approver = new Product { Id = "TessApprove", ParentId = "Root", Slogan = "Tin dung mua nha" };
            productlist.Add(approver);
            return RedirectToAction("Index");
        }

        public override ActionResult Reject(string destId)
        {
            throw new NotImplementedException();
        }
    }
}