﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Framework.Controllers
{
    public class Utility
    {
        public static void CheckSession(string key,object obj)
        {
            if (HttpContext.Current.Session["Approvemodel"] == null)
            {
                Dictionary<string, object> dicfake = new Dictionary<string, object>();
                HttpContext.Current.Session["Approvemodel"] = dicfake;
            }
            else
                ((Dictionary<string, object>)HttpContext.Current.Session["Approvemodel"]).Add(key, obj);
        }
    }
}