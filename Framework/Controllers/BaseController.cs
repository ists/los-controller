﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Framework.Controllers
{
    public abstract class BaseController<T> : Controller
    {
        //
        // GET: /Base/
        public abstract ActionResult Index();
        public abstract ActionResult GetDetail(string destId);
        public abstract ActionResult Insert(string destId);
        public abstract ActionResult Update(string destId);
        public abstract ActionResult Delete(string destId);
        public abstract ActionResult Save(T model);
        public abstract ActionResult GetById(string destId);
        public virtual ActionResult Approve(string destId) 
        { 
            throw new NotImplementedException(); 
        }

        public virtual ActionResult Reject(string destId)
        {
            throw new NotImplementedException(); 
        }
	}
}