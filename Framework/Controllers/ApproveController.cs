﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.Models;

namespace Framework.Controllers
{
    public class ApproveController : Controller
    {
      
        //
        // GET: /Approve/
        public ActionResult Index()
        {
            return View(Session["AuditList"]);
        }

        public ActionResult GetByID(string destId, string module)
        {
            ButtonModel button = new ButtonModel { RightName = "A", DestId = destId };
            ViewData["ButtonBar"] = button;
            string url = string.Format("~/Views/{0}/{0}DetailView.cshtml", module);

            var obj = ((Dictionary<string, object>)Session["Approvemodel"])[destId];
            return View(url, obj);
        }


        public ActionResult Approve(string destId)
        {
            var model =  ((List<AuditModel>)Session["AuditList"]).SingleOrDefault(x =>x.Id==destId);
            ((List<AuditModel>)Session["AuditList"]).Remove(model);
            string returnstring = string.Format("{0} has been approved", destId);
            return Content(returnstring);
        }
       
	}
}