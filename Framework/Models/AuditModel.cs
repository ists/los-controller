﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Framework.Models
{
    public class AuditModel
    {
        public string Id { get; set; }
        public string Module { get; set; }
        public string Slogan { get; set; }
    }
}