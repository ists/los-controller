﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Framework.Models
{
    public enum State { Allow, NotAllow };
    public enum Action { Insert, Update, Approve };
    public class BaseModel
    {
        public State StateApprove { get; set; }

        public Action UserAction { get; set; }

        public string Module { get; set; }


    }
}