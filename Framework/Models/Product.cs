﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Framework.Models
{
    public class Product
    {
        [Required]
        [StringLength(10,MinimumLength=2)]
        public string Id { get; set; }
        public string Slogan { get; set; }
        public string ParentId { get; set; }
    }
}